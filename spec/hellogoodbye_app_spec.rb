Capybara.default_driver = :selenium
# Capybara.default_driver = :poltergeist

# Capybara.javascript_driver = :poltergeist

options = {js_errors: false}

Capybara.app_host = "https://hello-goodbye-app.herokuapp.com/"

describe "Hello, goodbye App" do

  describe "visit root" do
    before { visit '/' }
    
    it "displays 'Greeter#hello' (default)" do
      p page.to_s
      # link = page.find(:css, 'a[href="https://www.google.com.mx/"]')
      # p "link"
      # p link
      # p link.to_s
      expect(page).to have_content 'Greeter#hello'
    end
    
    Capybara.default_selector = :xpath
    it "has a link to Google" do
      # expect(page).to have_selector(:css, 'a[href="www.google.com.mx"]')
#      link = page.find(:css, 'a[href="https://www.google.com.mx/"]')
      # p link
      # p link.to_s
      expect(page).to have_link("Google MX", href: 'https://www.google.com.mx/')
    end

    it "has a link to 'Goodbye section'" do
      expect(page).to have_link("Goodbye section", href: '/greeter/goodbye')
    end

    # Se puede validar la existencia de alguna tabla, su número de columnas, etc:
    # it "displays table element that has a row with 3 columns" do
    #   expect(page).to have_selector(:xpath, '//table//tr[count(td)=3]')
    # end

    # it "column 1 should have the thumbnail inside img tag" do
    #   expect(page).to have_selector(:xpath, '//table//tr/td[1]//img]')
    # end
  end

  it "has a link to 'Hello section'" do
    visit "greeter/goodbye"
    expect(page).to have_link("Hello section", href: '/greeter/hello')
  end
end